import { createContext } from "react";

const defaultValues = {
  activePage: null,
  updateActivePage: () => { },
  width: 0,
  height: 0
}

const AppContext = createContext(defaultValues)

export default AppContext
