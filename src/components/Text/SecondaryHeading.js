import React from 'react'

const SecondaryHeading = ({ children, marginTop, modifiers }) => {
  return (
    <h3 className={`h2 ${marginTop} ${modifiers}`}>
      {children}
    </h3>
  )
}

SecondaryHeading.defaultProps = {
  children: "Hey",
  marginTop: "",
  modifiers: ""
}

export default SecondaryHeading
