import React from 'react'

const Paragraph = ({ children, marginTop, modifiders }) => {
  return (
    <p className={`${marginTop} ${modifiders}`}>
      {children}
    </p>
  )
}

Paragraph.defaultProps = {
  children: "Hello!",
  marginTop: "m-xsmall-top-2",
  modifiders: ""
}

export default Paragraph
