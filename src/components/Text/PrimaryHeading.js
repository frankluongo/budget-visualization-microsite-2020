import React from 'react'

const PrimaryHeading = ({ headingText }) => {
  return (
    <h2 className="h1">{headingText}</h2>
  )
}

export default PrimaryHeading
