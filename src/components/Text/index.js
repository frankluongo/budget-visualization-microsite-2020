import IntroText from "./IntroText";
import Paragraph from "./Paragraph";
import PrimaryHeading from "./PrimaryHeading";
import QuarternaryHeading from "./QuarternaryHeading"
import QuinaryHeading from "./QuinaryHeading"
import SecondaryHeading from "./SecondaryHeading";
import TertiaryHeading from "./TertiaryHeading";

export { IntroText, Paragraph, PrimaryHeading, QuarternaryHeading, QuinaryHeading, SecondaryHeading, TertiaryHeading }
