import React from 'react'

const IntroText = ({ children, marginTop }) => {
  return (
    <p className={`${marginTop} large`}>
      <strong>
        {children}
      </strong>
    </p>
  )
}

IntroText.defaultProps = {
  children: "",
  marginTop: "m-xsmall-top-2"
}

export default IntroText
