import React from 'react'

const QuarternaryHeading = ({ children, marginTop, modifiers }) => {
  return (
    <h5 className={`h4 ${marginTop} ${modifiers}`}>
      {children}
    </h5>
  )
}

QuarternaryHeading.defaultProps = {
  children: "Hey",
  marginTop: "m-xsmall-top-2",
  modifiers: ""
}

export default QuarternaryHeading
