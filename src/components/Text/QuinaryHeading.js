import React from 'react'

const QuinaryHeading = ({ children, marginTop, modifiers }) => {
  return (
    <h6 className={`h5 ${marginTop} ${modifiers}`}>
      {children}
    </h6>
  )
}

QuinaryHeading.defaultProps = {
  children: "Hey",
  marginTop: "",
  modifiers: "m-xsmall-bottom-1"
}

export default QuinaryHeading
