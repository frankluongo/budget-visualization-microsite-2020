import React from 'react'

const TertiaryHeading = ({ children, marginTop, modifiers }) => {
  return (
    <h4 className={`h3 ${marginTop} ${modifiers}`}>
      {children}
    </h4>
  )
}

TertiaryHeading.defaultProps = {
  children: "Hey",
  marginTop: "m-xsmall-top-2",
  modifiers: ""
}

export default TertiaryHeading
