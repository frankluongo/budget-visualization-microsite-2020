import React, { useEffect, useContext, useRef, useState } from 'react'

import "./TableauEmbed.scss"
import AppContext from '../../context/AppContext'

import { TABLEAU_SCRIPT_SRC, WINDOW_THRESHOLD } from "../../constants"

const TableauEmbed = ({ aspectRatio, children, MobileImg, mobileLink, vizId, width }) => {
  const [showEmbed, updateEmbedDisplayState] = useState(false);
  const vizEmbed = useRef(null)
  const vizObject = useRef(null)
  const { width: windowWidth } = useContext(AppContext)
  const scriptElement = buildScriptElement();

  useEffect(handleEmbedDisplayStateChange, [showEmbed])

  return (
    <>
      {mobileLink.length > 0 && (
        <div className="viz-embed viz-embed--mobile">
          <a
            className="viz-embed__link link" href={mobileLink} target="_blank" rel="noopener noreferrer">
            {MobileImg && (
              <div className="m-xsmall-bottom-1">
                <MobileImg />
              </div>
            )
            }
            <div className="viz-embed__link-text">View Full Graphic</div>
          </a>
        </div>
      )}
      {showEmbed ? (
        <>
          <div ref={vizEmbed} className="viz-embed viz-embed--desktop" id={vizId} style={{ position: 'relative' }}>
            <object ref={vizObject} className='tableauViz' style={{ display: "none" }}>
              {children}
            </object>
          </div>
        </>
      ) : (
          <div
            className="viz-embed-placeholder"
            onClick={handleShowGraphicBtnClick}
            onKeyDown={(e) => e.key === "Enter" && handleShowGraphicBtnClick()}
            role="button"
            tabIndex="0"
          >
            <MobileImg />
            <button className="viz-embed-placeholder__toggle button primary default" onClick={handleShowGraphicBtnClick}>
              Click To Load Full Graphic
            </button>
          </div>
        )}
    </>
  )
  //
  // Handlers
  //
  function handleShowGraphicBtnClick() {
    updateEmbedDisplayState(true);
  }
  function handleEmbedDisplayStateChange() {
    if (vizEmbed.current && vizObject.current && windowWidth >= WINDOW_THRESHOLD) {
      sizeTableauGraphic();
      addTableauScriptToPage();
    }
  }
  //
  // Actions
  //
  function addTableauScriptToPage() {
    const scriptExists = !!document.querySelector(`script[src="${TABLEAU_SCRIPT_SRC}"]`)
    if (scriptExists) {
      removeTableauScript();
    }
    document.body.appendChild(scriptElement);
  }
  function buildScriptElement() {
    if (typeof document === 'undefined') return null
    const scriptEl = document.createElement('script');
    scriptEl.src = TABLEAU_SCRIPT_SRC;
    return scriptEl;
  }
  function removeTableauScript() {
    scriptElement.remove();
  }
  function sizeTableauGraphic() {
    vizObject.current.style.width = width;
    vizObject.current.style.height = `${(vizEmbed.current.offsetWidth * aspectRatio)}px`;
  }
}

TableauEmbed.defaultProps = {
  mobileLink: '',
  width: '100%'
}

export default TableauEmbed
