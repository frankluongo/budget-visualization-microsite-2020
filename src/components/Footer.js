import React from 'react'

const Footer = () => {
  return (
    <footer className="bg-gray-8 text-gray-1">
      <section className="container text-align-center p-xsmall-block-6">
        © {new Date().getFullYear()}, Colorado Legislative Council
      </section>
    </footer>
  )
}

export default Footer
