import React from 'react'
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import EmbedWrapper from '../../EmbedWrapper'
import TableauEmbed from '../../TableauEmbed'

const GraphicPlaceholderImg = () => {
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "tabor-dash.png" }) {
        childImageSharp {
          fluid(maxWidth: 900) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return <Img fluid={data.placeholderImage.childImageSharp.fluid} />
}

const TaborGraphic = () => {
  return (
    <EmbedWrapper>
      <article>
        <TableauEmbed
          aspectRatio={0.6}
          MobileImg={GraphicPlaceholderImg}
          mobileLink="https://public.tableau.com/views/TABOR_15812758722780/TABORDash?:display_count=y&publish=yes&:origin=viz_share_link"
          vizId="viz1581522338591"
        >
          <param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' /> <param name='embed_code_version' value='3' /> <param name='site_root' value='' /><param name='name' value='TABOR_15812758722780&#47;TABORDash' /><param name='tabs' value='no' /><param name='toolbar' value='yes' /><param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;TA&#47;TABOR_15812758722780&#47;TABORDash&#47;1.png' /> <param name='animate_transition' value='yes' /><param name='display_static_image' value='yes' /><param name='display_spinner' value='yes' /><param name='display_overlay' value='yes' /><param name='display_count' value='yes' /><param name='filter' value='publish=yes' />
        </TableauEmbed>
      </article>
    </EmbedWrapper>
  )
}

export default TaborGraphic
