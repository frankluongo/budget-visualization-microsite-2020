import React from 'react'
import Section from '../Section'

import TextBlock from "../TextBlock";
import { Paragraph, PrimaryHeading, SecondaryHeading, IntroText } from "../Text";

import data from "./whatIsTaborContent"
import TaborGraphic from './Visualizations/TaborGraphic';


const WhatIsTABOR = () => {

  return (
    <Section number={6} id="what-is-tabor">
      <TextBlock>
        <article>
          <PrimaryHeading headingText={data.heading} />
          <IntroText>
            {data.introText}
          </IntroText>
        </article>
      </TextBlock>
      <TextBlock>
        <article>
          <SecondaryHeading>
            {data.voterApprovHeading}
          </SecondaryHeading>
          <Paragraph>
            {data.voterApprovText}
          </Paragraph>
        </article>
      </TextBlock>
      <TextBlock>
        <article>
          <SecondaryHeading>
            {data.limitsHeading}
          </SecondaryHeading>
          <Paragraph>
            {data.limitsText}
          </Paragraph>
        </article>
      </TextBlock>
      <TaborGraphic />
    </Section>
  )
}

export default WhatIsTABOR
