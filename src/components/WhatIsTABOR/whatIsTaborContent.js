import React from "react";

const whatIsTaborContent = {
  heading: 'What is TABOR?',
  introText: 'The Taxpayer’s Bill of Rights (TABOR) Amendment is a constitutional amendment approved by voters in 1992.',
  voterApprovHeading: 'Voter Approval for Taxes',
  voterApprovText: 'The TABOR Amendment requires voter approval for tax increases.  Fees can be increased by the state legislature without voter approval.  Voter approval is also required to increase the TABOR limit, which constrains state revenue from both taxes and fees.',
  limitsHeading: 'Limits on State Revenue',
  limitsText: (
    <>
      The TABOR Amendment limits the amount of revenue the state can retain and spend, including revenue from most taxes and some fees. Any revenue that comes in above the limit is refunded to taxpayers. Prior to 2005, revenue was limited to the prior fiscal year’s revenue adjusted by Colorado inflation and population growth.
      <br />
      <br />
      In 2005, voters approved Referendum C, which allows the state to collect additional revenue up to a capped amount that grows by inflation plus population growth from FY 2007-08 revenue.
      <br />
      <br />
      <a className="link" href="https://leg.colorado.gov/publications/tabor-revenue-limit" target="_blank" rel="noopener noreferrer">Learn More About TABOR</a>
    </>
  )
}

export default whatIsTaborContent
