import React from 'react'

import "./EmbedWrapper.scss"

const EmbedWrapper = ({ children }) => {
  return (
    <section className="embed-wrapper">
      {children}
    </section>
  )
}

export default EmbedWrapper
