import React from 'react'

import Section from '../Section'
import TextBlock from '../TextBlock'

import { IntroText, Paragraph, PrimaryHeading, SecondaryHeading, TertiaryHeading } from "../Text";

import data from "./budgetBasicsContent";
import { Calendar, Capital, FundingCircle, Money, Scales } from '../Icons'

const BudgetBasics = () => {

  return (
    <Section number={2} id="budget-basics">

      <TextBlock>
        <aside>
          <Capital />
        </aside>
        <article>
          <PrimaryHeading headingText={data.heading} />
          <IntroText>
            {data.introLine}
          </IntroText>
          <Paragraph>
            {data.introParagraph}
          </Paragraph>
        </article>
      </TextBlock>

      <TextBlock>
        <aside>
          <Scales modifiers="fill-lcs-gold-web-4" />
        </aside>
        <article>
          <Paragraph>
            {data.balancedBudget}
          </Paragraph>
        </article>
      </TextBlock>


      <TextBlock>
        <aside>
          <Money />
        </aside>
        <article>
          <Paragraph>
            {data.stateBudget}
          </Paragraph>
        </article>
      </TextBlock>

      <TextBlock>
        <aside>
          <FundingCircle />
        </aside>
        <article>
          <SecondaryHeading>
            {data.budgetSourcesHeading}
          </SecondaryHeading>
          <IntroText>
            {data.budgetSourcesIntro}
          </IntroText>
          {data.basicBudgetSources.map((source, index) => (
            <div key={`budget-basics-source-${index}`}>
              <TertiaryHeading modifiers={source.color}>
                {source.heading}
              </TertiaryHeading>
              <Paragraph marginTop="m-xsmall-top-1">
                {source.text}
              </Paragraph>
            </div>
          ))}
        </article>
      </TextBlock>

      <TextBlock>
        <aside className="graphic-top">
          <Calendar />
        </aside>
        <article>
          <SecondaryHeading>
            {data.budgetTimelineHeading}
          </SecondaryHeading>
          <Paragraph>
            {data.budgetTimelineIntro}
          </Paragraph>
          <TertiaryHeading>
            {data.budgetPrepHeading}
          </TertiaryHeading>
          <Paragraph>
            {data.budgetPrepText}
          </Paragraph>
          <TertiaryHeading>
            {data.budgetBillsHeading}
          </TertiaryHeading>
          <Paragraph>
            {data.budgetBillsText}
          </Paragraph>
          <TertiaryHeading>
            {data.budgetOtherBillsHeading}
          </TertiaryHeading>
          <Paragraph>
            {data.budgetOtherBillsText}
          </Paragraph>
        </article>
      </TextBlock>
    </Section>
  )
}

export default BudgetBasics
