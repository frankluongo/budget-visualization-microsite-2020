import React from "react";
import ScrollLink from "../ScrollLink";

const budgetBasicsContent = {
  heading: 'Budget Basics',
  introLine: 'The state legislature determines the state budget.',
  introParagraph: 'The Governor, elected officials, and state agencies submit annual budget requests to the legislature.',
  balancedBudget: (
    <>
      Colorado must maintain a <strong>balanced budget</strong> each year, meaning that spending may not exceed the amount of tax and fee revenue that the state collects or saves.  In other words, unlike the federal government, the state cannot run a deficit.  With voter approval, the state can take on a limited amount of debt by issuing bonds that must be repaid over a set period of time.  The last time this occurred was in 1999 when <a className="link" href="http://hermes.cde.state.co.us/drupal/islandora/object/co:2578/datastream/OBJ/view" target="_blank" rel="noopener noreferrer" title="View the analysis of 1999 ballot approval">voters approved</a> $1.7 billion in bonds for transportation projects.
    </>
  ),
  stateBudget: (
    <>
      <strong>The state budget provides funding for state services, including health care, human services, higher education, state roads, and state courts and prisons.</strong>  Additionally, a portion of the state budget goes to local governments, including school districts, cities, and counties, to help fund K-12 education, local roads, and public assistance programs.  While the state provides a portion of this funding, these services are administered at the local government level.  Some local governments, such as the Regional Transportation District (RTD), are administered and funded locally without state support.
      <br />
      <br />
      <ScrollLink sectionLink="explore-state-revenue">
        Learn More About Where Funding Goes
      </ScrollLink>
    </>
  ),
  budgetSourcesHeading: 'Budget Sources',
  budgetSourcesIntro: 'Three major funds make up about a third of the state’s operating budget.',
  basicBudgetSources: [
    {
      color: "text-lcs-blue-4",
      heading: "General Funds",
      text: 'The most flexible source of funding is General Fund revenue, which is used for state programs including education, health care, human services, and corrections. The largest sources of General Fund revenue are income and sales taxes.'
    },
    {
      color: "text-lcs-gold-web-5",
      heading: "Cash Funds",
      text: 'Cash funds are required by law to be spent on certain programs (e.g., vehicle registration fees and the gas tax must be spent on transportation). Most revenue to cash funds comes from fees for government services.'
    },
    {
      color: "text-gray-6",
      heading: "Federal Funds",
      text: 'Federal funds include federal government funding for programs such as K-12 education, transportation, and assistance programs, such as Medicaid and Temporary Assistance for Needy Families.'
    },
  ],
  budgetTimelineHeading: 'Budget Timeline',
  budgetTimelineIntro: (<>The state fiscal year runs from July 1 through June 30 each year.<br />
    The state legislature sets next year’s budget during the current fiscal year.</>),
  budgetPrepHeading: 'Budget Preparation',
  budgetPrepText: (
    <>
      Departments work on their budgets year-round. The Governor, elected officials, and the judicial branch review proposals from the agencies that they oversee and submit their prioritized recommendations to the legislature by November 1.  Department budgets for day-to-day operations are presented to the <a className="link" href="https://leg.colorado.gov/committees/joint-budget-committee/2020-regular-session" target="_blank" rel="noopener noreferrer">Joint Budget Committee</a>, while major construction and information technology projects are presented to the <a className="link" href="https://leg.colorado.gov/committees/capital-development-committee/2020-regular-session" target="_blank" rel="noopener noreferrer">Capital Development</a> Committee and the <a className="link" href="https://leg.colorado.gov/committees/joint-technology-committee/2020-regular-session" target="_blank" rel="noopener noreferrer">Joint Technology Committee</a>. These committees prioritize requests made by departments, and the Joint Budget Committee introduces legislation reflecting its recommendations to the whole state legislature.
    </>
  ),
  budgetBillsHeading: 'Budget bills',
  budgetBillsText: 'The “Long Bill” and other major spending bills are typically introduced in late March.  The Long Bill includes funding for most state expenditures.  Like other bills, the state legislature amends and votes on the Long Bill before the end of the legislative session, which lasts 120 days from early January to early May.  Once a bill is passed, it must be approved by the Governor to become law. The Governor may veto the entire Long Bill or veto funding for specific purposes in the Long Bill (called a “line-item veto”).',
  budgetOtherBillsHeading: 'All other bills',
  budgetOtherBillsText: 'In addition to budget-related bills, a large portion of the bills introduced by the state legislature also require funding.  If a bill becomes law, this funding is added to the funding in the Long Bill and capital budget bills to form the operating and capital budgets.',
}

export default budgetBasicsContent
