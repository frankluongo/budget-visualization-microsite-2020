import React from "react"

const shareYourIdeasContent = {
  heading: 'Share Your Ideas & Further Reading',
  shareIdeasHeading: 'Share Your Budget Ideas',
  shareIdeasText: (
    <>
      Contact your legislator: <a className="link" href="https://leg.colorado.gov/findmylegislator" taget="_blank" rel="noopener noreferrer">Find My Legislator</a>
    </>
  ),
  legBudgetSourcesHeading: 'Budget Resources from Legislative Staff Agencies',
  legBudgetSources: [
    {
      description: "Nonpartisan staff analyses of department budgets",
      link: '//leg.colorado.gov/content/budget',
      title: "Joint Budget Committee Staff Documents",
    },
    {
      description: "A collection of staff research on the budget and taxes",
      link: '//leg.colorado.gov/agencies/legislative-council-staff/budget-taxes',
      title: "Fiscal Policy & Tax Research",
    },
    {
      description: "Forecasts of state revenue, selected caseloads, and the economy",
      link: '//leg.colorado.gov/agencies/legislative-council-staff/forecasting',
      title: "State Economic & Revenue Forecasts",
    },
    {
      description: "An overview of taxes collected in Colorado",
      link: '//leg.colorado.gov/agencies/legislative-council-staff/colorado-online-tax-handbook',
      title: "Online Tax Handbook",
    },
  ],
  otherSourcesHeading: 'Other Budget Resources',
  otherSources: [
    {
      description: "An interactive site tracking how the state is progressing on the Governor’s vision",
      link: '//dashboard.state.co.us/',
      title: "Governor’s Dashboard",
    },
    {
      description: "Includes a searchable database of state financial information provided by executive branch agencies",
      link: '//www.colorado.gov/transparency-online-project',
      title: "Transparency Online Project",
    },
    {
      description: "The state’s annual financial reports",
      link: '//www.colorado.gov/pacific/osc/cafr',
      title: "Colorado Comprehensive Annual Financial Reports",
    },
  ],
  stillQuestionsHeading: 'Still Have Questions?',
  stillQuestionsText: (
    <>
      Still have questions about the budget or comments about this website?
      <br />
      E-mail the staff of the Colorado General Assembly: <a className="link" href="mailto:ExploreStateBudget.GA@state.co.us" target="_blank" rel="noopener noreferrer">ExploreStateBudget.GA@state.co.us</a>
    </>
  )
}

export default shareYourIdeasContent
