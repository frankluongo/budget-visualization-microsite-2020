import React from 'react'

import Section from '../Section'

import TextBlock from "../TextBlock";
import { Paragraph, PrimaryHeading, SecondaryHeading, TertiaryHeading } from "../Text";

import { Share, OpenBook, Envelope } from '../Icons';

import data from "./furtherResourcesContent"


const FurtherResources = () => {

  return (
    <Section number={7} id="further-reading" modifiers="bg-gray-1">
      <TextBlock>
        <article>
          <PrimaryHeading headingText={data.heading} />
        </article>
      </TextBlock>
      <TextBlock>
        <aside>
          <Share modifiers="fill-lcs-gold-web-4" />
        </aside>
        <article>
          <SecondaryHeading>
            {data.shareIdeasHeading}
          </SecondaryHeading>
          <Paragraph>
            {data.shareIdeasText}
          </Paragraph>
        </article>
      </TextBlock>
      <TextBlock>
        <aside>
          <OpenBook modifiers="fill-lcs-gold-web-4" />
        </aside>
        <article>
          <SecondaryHeading>
            {data.legBudgetSourcesHeading}
          </SecondaryHeading>
          {data.legBudgetSources.map((item, index) => (
            <div key={`leg-budget-sources-${index}`}>
              <a className="link" href={item.link} target="_blank" rel="noopener noreferrer">
                <TertiaryHeading>
                  {item.title}
                </TertiaryHeading>
              </a>
              <Paragraph marginTop="m-xsmall-top-1">
                {item.description}
              </Paragraph>
            </div>
          ))}
        </article>
      </TextBlock>
      <TextBlock>
        <article>
          <SecondaryHeading>
            {data.otherSourcesHeading}
          </SecondaryHeading>
          {data.otherSources.map((item, index) => (
            <div key={`other-sources-${index}`}>
              <a className="link" href={item.link} target="_blank" rel="noopener noreferrer">
                <TertiaryHeading>
                  {item.title}
                </TertiaryHeading>
              </a>
              <Paragraph marginTop="m-xsmall-top-1">
                {item.description}
              </Paragraph>
            </div>
          ))}
        </article>
      </TextBlock>
      <TextBlock>
        <aside>
          <Envelope modifiers="fill-lcs-gold-web-4" />
        </aside>
        <article>
          <SecondaryHeading>
            {data.stillQuestionsHeading}
          </SecondaryHeading>
          <Paragraph>
            {data.stillQuestionsText}
          </Paragraph>
        </article>
      </TextBlock>
    </Section>
  )
}

export default FurtherResources
