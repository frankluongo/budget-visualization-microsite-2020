import React from "react"

const introTextContent = {
  heading: (<>Explore the<br />Colorado State Budget</>),
  subheading: (<>An in-depth introduction to how the budget is made<br />and how funds are distributed</>)
}

export default introTextContent
