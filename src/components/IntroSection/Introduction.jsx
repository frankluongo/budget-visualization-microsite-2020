import React from 'react'
import Section from '../Section'

import data from "./introSectionContent";

import "./introSection.scss"

const Introduction = () => {
  return (
    <Section modifiers="bg-lcs-blue-4" id="introduction">
      <div className="section-1__text">

        <h2 className="h1 large text-gray-1 text-align-center">{data.heading}</h2>
        <hr className="bg-lcs-blue-1" />
        <div className="p large text-gray-2 text-align-center margin-centered">
          <strong>
            {data.subheading}
          </strong>
        </div>
      </div>
    </Section>
  )
}

export default Introduction
