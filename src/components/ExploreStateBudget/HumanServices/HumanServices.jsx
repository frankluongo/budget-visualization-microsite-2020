import React from 'react'

import { Paragraph, QuarternaryHeading } from "../../Text";
import data from "./humanServicesContent"
import headings from "../budgetSectionHeadings"
import HumanServicesFundingGoes from './Visualizations/HumanServicesFundingGoes';
import HumanServicesDeptFunding from './Visualizations/HumanServicesDeptFunding';
import HumanServicesSlice from './Visualizations/HumanServicesSlice';


const HumanServices = () => {
  return (
    <>
      <Paragraph>
        {data.openingParagraphOne}
      </Paragraph>
      <div className="p m-xsmall-top-2">
        {data.openingList}
      </div>
      <Paragraph>
        {data.openingParagraphTwo}
      </Paragraph>

      <QuarternaryHeading>
        {headings.sliceOfBudget}
      </QuarternaryHeading>
      <HumanServicesSlice />

      <QuarternaryHeading>
        {headings.deptFunding}
      </QuarternaryHeading>
      <HumanServicesDeptFunding />

      <QuarternaryHeading>
        {headings.whereDoesFundingGo}
      </QuarternaryHeading>
      <Paragraph>
        {data.whereDoesFundingGoText}
      </Paragraph>
      <HumanServicesFundingGoes />

      <QuarternaryHeading>
        {headings.majorSpendingHeading}
      </QuarternaryHeading>
      <Paragraph>
        {data.majorSpendingParagraph}
      </Paragraph>
    </>
  )
}

export default HumanServices
