import React from "react"

const humanServicesContent = {
  openingParagraphOne: 'The Department of Human Services is responsible for administering and supervising all non-medical public assistance and welfare activities, including: ',
  openingList: (
    <ul className="list">
      <li>
        financial and food assistance;
      </li>
      <li>
        child welfare (protection from abuse and neglect) services;
      </li>
      <li>
        rehabilitation, mental health, and substance use treatment programs; and
      </li>
      <li>
        programs for the aging.
      </li>
    </ul>
  ),
  openingParagraphTwo: "The department also inspects and licenses care and treatment facilities for children, people seeking behavioral health services, people with developmental disabilities, and juvenile offender populations.  Additionally, the department operates two psychiatric hospitals, several facilities and group homes for persons with developmental disabilities, and ten institutions for juvenile offenders.",
  whereDoesFundingGoText: "State operating funds for the department are allocated across the many programs supported by the department.",
  majorSpendingParagraph: (
    <>
      Each of the major divisions in the department have unique spending drivers, depending on population growth, eligibility requirements for assistance programs, administrative decisions, and economic conditions, among other factors.
      <br />
      <br />
      <a className="link" href="https://leg.colorado.gov/content/budget" target="_blank" rel="noopener noreferrer">More on the Human Services Budget</a>
    </>
  )
}

export default humanServicesContent
