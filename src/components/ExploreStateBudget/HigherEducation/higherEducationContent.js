import React from "react"

const higherEducationContent = {
  openingParagraph: "The Department of Higher Education is responsible for administering the state’s colleges and universities.",
  majorSpendingParagraphOne: "Student enrollment is one driver of spending on higher education. Enrollment tends to rise during economic downturns when job opportunities are limited. Colorado residents make up the largest share of students.  Other spending drivers include staff compensation, student support initiatives, and information technology and building costs.",
  majorSpendingParagraphTwo: (
    <>
      State funding for higher education from the General Fund has declined over time relative to cost of services provided due to General Fund budgetary pressures.  State colleges and universities have increased tuition rates to offset reductions in General Fund contributions and rising costs of services.  Tuition costs are higher for nonresident students.
      <br />
      <br />
      <a className="link" href="https://leg.colorado.gov/content/budget" target="_blank" rel="noopener noreferrer">
        More on the Higher Education Budget
      </a>
    </>
  )
}

export default higherEducationContent
