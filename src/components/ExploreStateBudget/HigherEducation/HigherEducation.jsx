import React from 'react'

import { Paragraph, QuarternaryHeading } from "../../Text";

import data from "./higherEducationContent"
import headings from "../budgetSectionHeadings"
import HigherEducationSlice from './Visualizations/HigherEducationSlice';
import HigherEducationDeptFunding from './Visualizations/HigherEducationDeptFunding';
import HigherEducationFundingGoes from './Visualizations/HigherEducationFundingGoes';
import HigherEducationStudentEnrollment from './Visualizations/HigherEducationStudentEnrollment';
import HigherEducationSources from './Visualizations/HigherEducationSources';

const HigherEducation = () => {
  return (
    <>
      <Paragraph>
        {data.openingParagraph}
      </Paragraph>
      <QuarternaryHeading>
        {headings.sliceOfBudget}
      </QuarternaryHeading>
      <HigherEducationSlice />

      <QuarternaryHeading>
        {headings.deptFunding}
      </QuarternaryHeading>
      <HigherEducationDeptFunding />

      <QuarternaryHeading>
        {headings.whereDoesFundingGo}
      </QuarternaryHeading>
      <HigherEducationFundingGoes />


      <QuarternaryHeading>
        {headings.majorSpendingHeading}
      </QuarternaryHeading>

      <Paragraph>
        {data.majorSpendingParagraphOne}
      </Paragraph>
      <HigherEducationStudentEnrollment />

      <Paragraph>
        {data.majorSpendingParagraphTwo}
      </Paragraph>
      <HigherEducationSources />
    </>
  )
}

export default HigherEducation
