import React from 'react'
import Section from '../Section'

import TextBlock from "../TextBlock";
import { Paragraph, PrimaryHeading, SecondaryHeading } from "../Text";
import Accordion from "../Accordion"

import data from "./exploreTheBudgetContent";
import StateSpendingGraphic from './StateSpendingGraphic';
import StateOperatingBudget from './Visualizations/StateOperatingBudget';

const ExploreStateBudget = () => {

  return (
    <Section number={5} id="explore-the-state-budget">
      <TextBlock>
        <aside className="img-wrapper">
          <div className="p-xsmall-right-2 m-xsmall-top-2">
            <div className="p-xsmall-left-6">
              <StateSpendingGraphic />
            </div>
          </div>
        </aside>
        <article>
          <PrimaryHeading headingText={data.heading} />
          <Paragraph>
            {data.introText}
          </Paragraph>
        </article>
      </TextBlock>

      <TextBlock>
        <article>
          <SecondaryHeading>
            {data.opBudgetHeading}
          </SecondaryHeading>
          <Paragraph>
            {data.opBudgetText}
          </Paragraph>
        </article>
      </TextBlock>

      <StateOperatingBudget />

      <TextBlock>
        <article>
          <SecondaryHeading>
            {data.theBigSixHeading}
          </SecondaryHeading>
          <Paragraph>
            {data.theBigSixText}
          </Paragraph>
          <Accordion
            modifiers="m-xsmall-top-2"
            panels={data.stateBudgetDepts}
          />
          <SecondaryHeading marginTop="m-xsmall-top-2">
            {data.transportFundingHeading}
          </SecondaryHeading>
          <Paragraph>
            {data.transportationFundingText}
          </Paragraph>
          <Accordion
            modifiers="m-xsmall-top-2"
            panels={data.transportation}
          />
          <SecondaryHeading marginTop="m-xsmall-top-2">
            {data.capBudgetHeading}
          </SecondaryHeading>
          <Paragraph>
            {data.capBudgetText}
          </Paragraph>
          <Accordion
            modifiers="m-xsmall-top-2"
            panels={data.capConstruction}
          />
        </article>
      </TextBlock>
    </Section>
  )
}

export default ExploreStateBudget
