import React from 'react'

import { Paragraph, QuarternaryHeading } from "../../Text";
import headings from "../budgetSectionHeadings";
import data from "./correctionsContent";
import CorrectionsSlice from './Visualizations/CorrectionsSlice';
import CorrectionsDeptFunding from './Visualizations/CorrectionsDeptFunding';
import CorrectionsFundingGoes from './Visualizations/CorrectionsFundingGoes';
import CorrectionsPrisonPop from './Visualizations/CorrectionsPrisonPop';

const Corrections = () => {
  return (
    <>
      <Paragraph>
        {data.openingParagraph}
      </Paragraph>
      <div className="p m-xsmall-top-2">
        {data.openingList}
      </div>

      <QuarternaryHeading>
        {headings.sliceOfBudget}
      </QuarternaryHeading>
      <CorrectionsSlice />


      <QuarternaryHeading>
        {headings.deptFunding}
      </QuarternaryHeading>
      <CorrectionsDeptFunding />


      <QuarternaryHeading>
        {headings.whereDoesFundingGo}
      </QuarternaryHeading>
      <CorrectionsFundingGoes />

      <QuarternaryHeading>
        {headings.majorSpendingHeading}
      </QuarternaryHeading>
      <Paragraph>
        {data.majorSpendingParagraph}
      </Paragraph>
      <CorrectionsPrisonPop />
    </>
  )
}

export default Corrections
