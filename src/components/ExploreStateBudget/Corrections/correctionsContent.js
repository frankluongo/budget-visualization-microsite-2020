import React from "react"

const correctionsContent = {
  openingParagraph: 'The Department of Corrections is responsible for: ',
  openingList: (
    <ul className="list">
      <li>
        managing Colorado’s state correctional facilities;
      </li>
      <li>
        supervising the population of offenders in the custody of the department; and
      </li>
      <li>
        developing educational, treatment, and correctional industries programs that have a rehabilitative or therapeutic value for inmates.
      </li>
    </ul>
  ),
  majorSpendingParagraph: (
    <>
      The number of inmates and parolees is the primary driver of spending at the department.  Many factors contribute to the size of this population, including state legislation that sets the criminal code, and decisions made by prosecutors, judges, the Parole Board, and inmates.
    <br />
      <br />
      <a className="link" href="https://leg.colorado.gov/content/budget" target="_blank" rel="noopener noreferrer">More on the Corrections Budget</a>
    </>
  )
}

export default correctionsContent
