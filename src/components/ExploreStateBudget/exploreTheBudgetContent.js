import React from "react";
import { TertiaryHeading } from '../Text'
import Corrections from "./Corrections"
import HealthCare from "./HealthCare"
import HigherEducation from "./HigherEducation"
import HumanServices from "./HumanServices"
import Judicial from "./Judicial"
import K12Education from "./K12Education"
import Transportation from "./Transportation"
import CapitalConstruction from "./CapitalConstruction"

const exploreTheBudgetContent = {
  heading: 'Explore the State Budget',
  introText: 'When most people talk about the budget, they are referring to the state budget, which is determined by the state legislature. The state budget includes funding for day-to-day operations and for capital spending. Other forms of state spending are not included in the budget.  These funds primarily include federal funds for food assistance programs and some federal and private funding for higher education research projects.',
  opBudgetHeading: 'The Operating Budget',
  opBudgetText: 'The state legislature determines the state operating budget, which is used to fund the day-to-day operations of state government.',
  theBigSixHeading: 'The Big Six',
  theBigSixText: 'A majority of state operating funds go to six state departments',
  transportFundingHeading: 'Transportation Funding',
  transportationFundingText: (
    <>
      Funding for transportation projects is unique.  Part of this funding shows up in the operating budget, but that is not the whole story.
      <br />
      <br />
      For more details about other departments, <a className="link" href="//leg.colorado.gov/publications/appropriations-report-fiscal-year-2019-20" target="_blank" rel="noopener noreferrer">see the FY 2019-20 Appropriations Report</a>.
    </>
  ),
  capBudgetHeading: 'The Capital Budget',
  capBudgetText: 'In addition to the operating budget, the state has a budget for capital construction and major information technology projects that is determined by the state legislature.',
  capConstructionHeading: 'Capital Construction & Information Technology',
  stateBudgetDepts: [
    {
      title: (
        <TertiaryHeading marginTop="">
          Health Care
        </TertiaryHeading>
      ),
      content: (
        <HealthCare />
      )
    },
    {
      title: (
        <TertiaryHeading marginTop="">
          K-12 Education
        </TertiaryHeading>
      ),
      content: (
        <K12Education />
      )
    },
    {
      title: (
        <TertiaryHeading marginTop="">
          Higher Education
        </TertiaryHeading>
      ),
      content: (
        <HigherEducation />
      )
    },
    {
      title: (
        <TertiaryHeading marginTop="">
          Human Services
        </TertiaryHeading>
      ),
      content: (
        <HumanServices />
      )
    },
    {
      title: (
        <TertiaryHeading marginTop="">
          Corrections
        </TertiaryHeading>
      ),
      content: (
        <Corrections />
      )
    },
    {
      title: (
        <TertiaryHeading marginTop="">
          Judicial
        </TertiaryHeading>
      ),
      content: (
        <Judicial />
      )
    },
  ],
  transportation: [
    {
      title: (
        <TertiaryHeading marginTop="">
          Transportation
        </TertiaryHeading>
      ),
      content: (
        <Transportation />
      )
    }
  ],
  capConstruction: [
    {
      title: (
        <TertiaryHeading marginTop="">
          Capital Construction & Information Technology
        </TertiaryHeading>
      ),
      content: (
        <CapitalConstruction />
      )
    }
  ],
}

export default exploreTheBudgetContent
