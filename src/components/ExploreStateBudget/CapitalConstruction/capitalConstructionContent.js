import React from "react"

const capitalConstructionContent = {
  openingParagraph: 'State agencies own 2,387 buildings statewide, with a current replacement value of $13.3 billion.',
  budgetProcessHeading: 'Budget Process',
  budgetProcessParagraph: (
    <>
      Building projects and repairs are prioritized by the <a className="link" href="https://leg.colorado.gov/committees/capital-development-committee/2020-regular-session" target="_blank" rel="noopener noreferrer">Capital Development Committee</a>, while major information technology projects are reviewed by the <a className="link" href="https://leg.colorado.gov/committees/joint-technology-committee/2020-regular-session" target="_blank" rel="noopener noreferrer">Joint Technology Committee</a>. These committees make recommendations to the <a className="link" href="https://leg.colorado.gov/committees/joint-budget-committee/2020-regular-session" target="_blank" rel="noopener noreferrer">Joint Budget Committee</a>, which then introduces legislation reflecting recommendations to the whole state legislature.
    </>
  ),
  stateFundingHeading: 'State Funding',
  stateFundingParagraph: 'General Fund transfers to the Capital Construction Fund make up a majority of funding for capital projects.',
  whereDoesFundingGoParagraph: 'In recent years, a large share of capital construction funding has gone to state colleges and universities. These projects are primarily financed with cash fund revenue from college- and university-related services, such as parking revenues, dorm fees, and athletic event revenues.',
  majorSpendingParagraph: (
    <>
      Population growth, demand for state services, the age of state infrastructure, and available funds within the operating budget are major spending drivers for state capital construction and information technology.
      <br />
      <br />
      State agencies have become more reliant on technology over time, leading to rising information technology costs. Additionally, as buildings age, they require ongoing repairs and maintenance. When funding is put off for building repairs and maintenance, future construction costs tend to be higher.
      <br />
      <br />
      <a className="link" href="https://leg.colorado.gov/content/budget" target="_blank" rel="noopener noreferrer">More on the Capital Budget</a>
    </>
  )
};

export default capitalConstructionContent
