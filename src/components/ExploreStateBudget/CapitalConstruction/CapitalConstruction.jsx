import React from 'react'

import { Paragraph, QuarternaryHeading } from "../../Text";
import data from "./capitalConstructionContent"
import headings from "../budgetSectionHeadings"
import CapBudgetSlice from './Visualizations/CapBudgetSlice';
import CapStateFunding from './Visualizations/CapStateFunding';
import CapFundingGoes from './Visualizations/CapFundingGoes';

const CapitalConstruction = () => {
  return (
    <>
      {/* Intro */}
      <Paragraph>
        {data.openingParagraph}
      </Paragraph>
      <CapBudgetSlice />

      {/* Budget Process */}
      <QuarternaryHeading>
        {data.budgetProcessHeading}
      </QuarternaryHeading>
      <Paragraph>
        {data.budgetProcessParagraph}
      </Paragraph>

      {/* State Funding */}
      <QuarternaryHeading>
        {data.stateFundingHeading}
      </QuarternaryHeading>
      <Paragraph>
        {data.stateFundingParagraph}
      </Paragraph>
      <CapStateFunding />

      {/* Where Does Funding Go? */}
      <QuarternaryHeading>
        {headings.whereDoesFundingGo}
      </QuarternaryHeading>
      <Paragraph>
        {data.whereDoesFundingGoParagraph}
      </Paragraph>
      <CapFundingGoes />

      {/* Major Spending Drivers */}
      <QuarternaryHeading>
        {headings.majorSpendingHeading}
      </QuarternaryHeading>
      <Paragraph>
        {data.majorSpendingParagraph}
      </Paragraph>
    </>
  )
}

export default CapitalConstruction
