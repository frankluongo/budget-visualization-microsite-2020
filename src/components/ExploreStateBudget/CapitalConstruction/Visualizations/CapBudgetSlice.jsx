import React from 'react'

import EmbedWrapper from '../../../EmbedWrapper'
import TableauEmbed from '../../../TableauEmbed'

import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
const GraphicPlaceholderImg = () => {
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "capital-budget-slice.png" }) {
        childImageSharp {
          fluid(maxWidth: 900) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return <Img fluid={data.placeholderImage.childImageSharp.fluid} />
}

const CapBudgetSlice = () => {
  return (
    <EmbedWrapper>
      <article className="full">
        <TableauEmbed
          aspectRatio={0.35}
          mobileLink="https://public.tableau.com/views/CapitalBudget_15813515976190/CAPD1?:display_count=y&publish=yes&:origin=viz_share_link"
          MobileImg={GraphicPlaceholderImg}
          vizId="viz1581610016124"
        >
          <param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' /> <param name='embed_code_version' value='3' /> <param name='site_root' value='' /><param name='name' value='CapitalBudget_15813515976190&#47;CAPD1' /><param name='tabs' value='no' /><param name='toolbar' value='yes' /><param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;Ca&#47;CapitalBudget_15813515976190&#47;CAPD1&#47;1.png' /> <param name='animate_transition' value='yes' /><param name='display_static_image' value='yes' /><param name='display_spinner' value='yes' /><param name='display_overlay' value='yes' /><param name='display_count' value='yes' /><param name='filter' value='publish=yes' />
        </TableauEmbed>
      </article>
    </EmbedWrapper>
  )
}

export default CapBudgetSlice
