const budgetSectionHeadings = {
  sliceOfBudget: 'Slice of The State Budget',
  deptFunding: 'Department Funding',
  whereDoesFundingGo: 'Where does the funding go?',
  majorSpendingHeading: 'Major Spending Drivers',
}

export default budgetSectionHeadings
