import React from 'react'

import { Paragraph, QuarternaryHeading } from "../../Text";
import HealthCareBudgetSlice from './Visualizations/HealthCareBudgetSlice';
import HealthCareFunding from './Visualizations/HealthCareFunding';
import HealthCareFundingGoes from './Visualizations/HealthCareFundingGoes';
import MedicardEnrollment from './Visualizations/MedicardEnrollment';
import MedicardExpenditures from './Visualizations/MedicardExpenditures';

import data from "./healthCareContent"
import headings from "../budgetSectionHeadings";

const HealthCare = () => {
  return (
    <>
      <Paragraph>
        {data.openingPargraph}
      </Paragraph>
      <QuarternaryHeading>
        {headings.sliceOfBudget}
      </QuarternaryHeading>
      <HealthCareBudgetSlice />
      <QuarternaryHeading>
        {headings.deptFunding}
      </QuarternaryHeading>
      <HealthCareFunding />
      <QuarternaryHeading>
        {headings.whereDoesFundingGo}
      </QuarternaryHeading>
      <HealthCareFundingGoes />
      <QuarternaryHeading>
        {headings.majorSpendingHeading}
      </QuarternaryHeading>
      <Paragraph>
        {data.majorSpendingParagraphOne}
      </Paragraph>
      <MedicardEnrollment />
      <Paragraph>
        {data.majorSpendingParagraphTwo}
      </Paragraph>
      <MedicardExpenditures />
    </>
  )
}

export default HealthCare
