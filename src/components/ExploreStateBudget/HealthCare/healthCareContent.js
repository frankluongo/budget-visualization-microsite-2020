import React from "react"

const healthCareContent = {
  openingPargraph: 'The Department of Health Care Policy and Financing helps pay health and long-term care expenses for low-income and vulnerable populations.',
  majorSpendingParagraphOne: 'The number of people enrolled in Medicaid is the largest driver of state health care spending.  Enrollment tends to rise during economic downturns.  State and federal law determine who is eligible for Medicaid and what services are covered.',
  majorSpendingParagraphTwo: (
    <>
      Health care costs are another factor influencing spending. The elderly and people with disabilities typically require higher levels of spending among the populations eligible for Medicaid.
      <br />
      <br />
      <a
        className="link"
        href="//leg.colorado.gov/content/budget/budget-documents"
        rel="noopener noreferrer"
        target="_blank"
      >
        More on the Healthcare Budget
      </a>
    </>
  )
}

export default healthCareContent
