import React from 'react'

import { Paragraph, QuarternaryHeading } from "../../Text";
import headings from "../budgetSectionHeadings";
import data from "./transportationContent"
import TransportationList from './TransportationList';
import TransportationSlice from './Visualizations/TransportationSlice';
import TransportationDeptFunding from './Visualizations/TransportationDeptFunding';
import TransportationFundingGoes from './Visualizations/TransportationFundingGoes';
import TransportationFundingHistory from './Visualizations/TransportationFundingHistory';

const Transportation = () => {
  return (
    <>
      {/* Intro Section */}
      <Paragraph>
        {data.openingParagraph}
      </Paragraph>
      <section className="row m-xsmall-top-2">
        <TransportationList list={data.stateHighwayList} />
        <TransportationList list={data.localRoadsList} modifiers="m-xsmall-top-2 m-medium-top-0" />
      </section>
      <section className="row m-xsmall-top-2">
        <TransportationList list={data.busesList} />
        <TransportationList list={data.airportList} modifiers="m-xsmall-top-2 m-medium-top-0" />
      </section>
      <section className="m-xsmall-top-1 m-xsmall-bottom-3">
        {data.openingLink}
      </section>
      {/* Slice of The Budget */}
      <QuarternaryHeading>
        {headings.sliceOfBudget}
      </QuarternaryHeading>
      <TransportationSlice />
      {/* Department Funding */}
      <QuarternaryHeading>
        {headings.deptFunding}
      </QuarternaryHeading>
      <TransportationDeptFunding />
      <Paragraph>
        {data.deptFundingParagraph}
      </Paragraph>
      {/* Where Does The Funding Go? */}
      <QuarternaryHeading>
        {headings.whereDoesFundingGo}
      </QuarternaryHeading>
      <Paragraph>
        {data.whereDoesFundingGoParagraph}
      </Paragraph>
      <TransportationFundingGoes />
      {/* Major Spending Drivers */}
      <QuarternaryHeading>
        {headings.majorSpendingHeading}
      </QuarternaryHeading>
      <Paragraph>
        {data.majorSpendingParagraph}
      </Paragraph>
      <TransportationFundingHistory />
    </>
  )
}

export default Transportation
