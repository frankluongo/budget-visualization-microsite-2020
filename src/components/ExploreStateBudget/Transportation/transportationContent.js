import React from "react"

const transportationContent = {
  openingParagraph: 'Highways, roads, and other transportation infrastructure are funded through a combination of federal, state, and local government revenue.',
  stateHighwayList: {
    title: 'State Highway System',
    items: [
      'Federal gas tax',
      'State gas tax',
      'Vehicle registration fees',
      'General Fund transfers'
    ]
  },
  localRoadsList: {
    title: 'Local Roads',
    items: [
      'State distributions of funding from state and federal sources',
      'Specific ownership tax',
      'Local sales taxes, property taxes, and fees'
    ]
  },
  busesList: {
    title: 'Buses & Mass Transit',
    items: [
      'State distributions of funding from state and federal sources',
      'Local sales taxes, property taxes, and fees',
      'Local sales taxes, property taxes, and fees'
    ]
  },
  airportList: {
    title: 'Airports',
    items: [
      'Federal funding and grants',
      'Specific ownership tax',
      'Bus and transit fares'
    ]
  },
  openingLink: (
    <a className="link" href="https://leg.colorado.gov/publications/colorados-transportation-system" target="_blank" rel="noopener noreferrer">More on the transportation budget</a>
  ),
  deptFundingParagraph: 'A majority of state funding for transportation comes from cash funds and federal funds. In some years, voters approved bonds to pay for large transportation projects. Additionally, the state has transferred General Funds to transportation in some years. Bonds and transfers fall outside of the state operating budget.',
  whereDoesFundingGoParagraph: 'Most of the funding from the state operating budget is allocated for the construction, maintenance, and operations of transportation systems. These dollars help to fund highways, roads, mass transit, and airports throughout the state.',
  majorSpendingParagraph: (
    <>
      Most state funding for transportation comes from the gas tax and vehicle registration fees. Revenue from these sources tends to grow with fuel consumption and the state population, instead of general economic trends. As vehicles have become more fuel-efficient and cars are lasting longer, fuel taxes and vehicle registration fees have grown less quickly than vehicle miles traveled.
      <br />
      <br />
      Rising construction costs and aging roads and bridges also contribute to budgetary pressures for state and local governments.
      <br />
      <br />
      <a className="link" href="https://leg.colorado.gov/content/budget" target="_blank" rel="noopener noreferrer">More on the transportation budget</a>
      <br />
      <a className="link" href="https://www.codot.gov/business/budget/cdot-budget" target="_blank" rel="noopener noreferrer">CDOT Annual Reports &amp; Information</a>
    </>
  )
}

export default transportationContent
