import React from 'react'
import EmbedWrapper from '../../../EmbedWrapper'
import TableauEmbed from '../../../TableauEmbed'
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
const GraphicPlaceholderImg = () => {
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "transportation-funding-history.png" }) {
        childImageSharp {
          fluid(maxWidth: 900) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return <Img fluid={data.placeholderImage.childImageSharp.fluid} />
}

const TransportationFundingHistory = () => {
  return (
    <EmbedWrapper>
      <article className="full">
        <TableauEmbed
          mobileLink="https://public.tableau.com/views/RealTransportationFundingHistory/CDOTD2?:display_count=y&:origin=viz_share_link"
          MobileImg={GraphicPlaceholderImg} aspectRatio={0.4} vizId="viz1581617329124">
          <param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' /> <param name='embed_code_version' value='3' /> <param name='site_root' value='' /><param name='name' value='RealTransportationFundingHistory&#47;CDOTD2' /><param name='tabs' value='no' /><param name='toolbar' value='yes' /><param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;Re&#47;RealTransportationFundingHistory&#47;CDOTD2&#47;1.png' /> <param name='animate_transition' value='yes' /><param name='display_static_image' value='yes' /><param name='display_spinner' value='yes' /><param name='display_overlay' value='yes' /><param name='display_count' value='yes' />
        </TableauEmbed>
      </article>
    </EmbedWrapper>
  )
}

export default TransportationFundingHistory
