import React from 'react'
import { QuinaryHeading } from '../../Text'

const TransportationList = ({ list, modifiers }) => {
  return (
    <div className={`col-half ${modifiers}`}>
      <QuinaryHeading>
        {list.title}
      </QuinaryHeading>
      <ul className="list small">
        {list.items.map((item, index) => (
          <li key={index}>
            {item}
          </li>
        ))}
      </ul>
    </div>
  )
}

TransportationList.defaultProps = {
  modifiers: ""
}

export default TransportationList
