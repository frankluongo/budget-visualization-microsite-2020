import React from 'react'

import { Paragraph, QuarternaryHeading } from "../../Text";

import data from "./k12EducationContent";
import headings from "../budgetSectionHeadings";

import K12BudgetSlice from './Visualizations/K12EducationBudgetSlice';
import K12Funding from './Visualizations/K12Funding';
import K12FundingGoes from './Visualizations/K12FundingGoes';
import K12ProgramFunding from './Visualizations/K12ProgramFunding';
import K12ProgramTotal from './Visualizations/K12ProgramTotal';

const K12Education = () => {
  return (
    <>
      {/* Intro  */}
      <Paragraph>
        {data.openingPargraph}
      </Paragraph>
      {/* Slice of The Budget */}
      <QuarternaryHeading>
        {headings.sliceOfBudget}
      </QuarternaryHeading>
      <K12BudgetSlice />
      {/* Department Funding */}
      <QuarternaryHeading>
        {headings.deptFunding}
      </QuarternaryHeading>
      <K12Funding />
      {/* Where Does The Funding Go? */}
      <QuarternaryHeading>
        {headings.whereDoesFundingGo}
      </QuarternaryHeading>
      <K12FundingGoes />
      {/* Major Spending Drivers */}
      <QuarternaryHeading>
        {headings.majorSpendingHeading}
      </QuarternaryHeading>
      <Paragraph>
        {data.majorSpendingParagraphOne}
      </Paragraph>
      <div className="p m-xsmall-top-2">
        {data.majorSpendingList}
      </div>
      <K12ProgramFunding />
      <Paragraph>
        {data.majorSpendingParagraphTwo}
      </Paragraph>
      <K12ProgramTotal />
    </>
  )
}

export default K12Education
