import React from "react"

const k12Content = {
  openingPargraph: 'Most of the state funding for K-12 education goes directly to the state’s 178 school districts to share the cost of providing public education with local school districts.  The Department of Education also administers licenses and administers a number of education-related programs.',
  majorSpendingParagraphOne: (
    <>
      State law includes a formula that determines how much is allocated to each school district.  This formula accounts for the number of students and cost of living in each school district, and inflationary pressures, among other factors.
      <br />
      <br />
      <a
        className="link"
        href="https://leg.colorado.gov/publications/school-finance-colorado-2019"
        rel="noopener noreferrer"
        target="_blank"
      >
        More on the School Finances Formula
      </a>
      <br />
      <br />
      The state and school districts share the cost of providing public education.  The <strong>state share</strong> comes primarily from the General Fund, while most school district funding comes from property taxes.  The state share has increased over time relative to the local share due to:
    </>
  ),
  majorSpendingList: (
    <>
      <ul className="list">
        <li>
          the spending requirements of Amendment 23, which was adopted by voters in 2000 and requires appropriations to increase by enrollment and inflation;
        </li>
        <li>
          the impacts of the Gallagher Amendment, which was adopted in 1982 and restricts growth in property taxes; and
        </li>
        <li>
          the TABOR Amendment, which imposes limits on state and local government revenue and requires voters to approve new or increased taxes.
        </li>
      </ul>
      <br />
      <a
        className="link"
        href="https://leg.colorado.gov/publications/school-finance-and-constitution-0"
        rel="noopener noreferrer"
        target="_blank"
      >
        More on these Dynamics
      </a>
    </>
  ),
  majorSpendingParagraphTwo: (
    <>
      Following the 2007-09 recession, the state legislature created the <strong>Budget Stabilization Factor</strong> to alleviate some of the budgetary pressures facing the state. The Budget Stabilization Factor proportionally reduces the amount of funding across each school district.  As of FY 2019-20, the factor reduced funding by 7 percent.
      <br />
      <br />
      <a
        className="link"
        href="https://leg.colorado.gov/content/budget"
        rel="noopener noreferrer"
        target="_blank"
      >
        More on the education budget
      </a>
    </>
  )
}

export default k12Content
