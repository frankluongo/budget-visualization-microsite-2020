import React from 'react'

import { Paragraph, QuarternaryHeading } from "../../Text";
import data from "./judicialContent"
import headings from "../budgetSectionHeadings";
import JudicialSlice from './Visualizations/JudicialSlice';
import JudicialDeptFunding from './Visualizations/JudicialDeptFunding';
import JudicialFundingGoes from './Visualizations/JudicialFundingGoes';
import JudicialCaseloadHistory from './Visualizations/JudicialCaseloadHistory';


const Judicial = () => {
  return (
    <>
      <div className="p">
        {data.openingList}
      </div>

      <QuarternaryHeading>
        {headings.sliceOfBudget}
      </QuarternaryHeading>
      <JudicialSlice />


      <QuarternaryHeading>
        {headings.deptFunding}
      </QuarternaryHeading>
      <JudicialDeptFunding />


      <QuarternaryHeading>
        {headings.whereDoesFundingGo}
      </QuarternaryHeading>
      <Paragraph>
        {data.whereDoesFundingGoParagraph}
      </Paragraph>
      <JudicialFundingGoes />

      <QuarternaryHeading>
        {headings.majorSpendingHeading}
      </QuarternaryHeading>
      <Paragraph>
        {data.majorSpendingParagraph}
      </Paragraph>
      <JudicialCaseloadHistory />
    </>
  )
}

export default Judicial
