import React from "react"

const judicialContent = {
  openingList: (
    <>
      The Judicial Department consists of:
      <br />
      <ul className="list">
        <li>the state courts;</li>
        <li>probation, which supervises juvenile and adult offenders who have been sentenced to probation rather than jail or prison; and</li>
        <li>several agencies that provide legal representation in court for children and low-income populations.</li>
      </ul>
    </>
  ),
  whereDoesFundingGoParagraph: 'The largest share of spending goes to the administration of the state’s courts.',
  majorSpendingParagraph: (
    <>
      The primary drivers of spending for the department are the number of people on probation and the number and type of court cases. Some court cases, such as felony cases, are much more time-consuming than others.
      <br />
      <br />
      <a className="link" href="https://leg.colorado.gov/content/budget" target="_blank" rel="noopener noreferrer">More on the Judicial Budget</a>
    </>
  )
}

export default judicialContent
