import React from 'react'
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import EmbedWrapper from "../../EmbedWrapper";
import TableauEmbed from "../../TableauEmbed";

const StateOperatingBudgetPlaceholder = () => {
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "budget-story.png" }) {
        childImageSharp {
          fluid(maxWidth: 900) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return <Img fluid={data.placeholderImage.childImageSharp.fluid} />
}

const StateOperatingBudget = () => {
  return (
    <EmbedWrapper>
      <article>
        <TableauEmbed
          aspectRatio={0.77}
          MobileImg={StateOperatingBudgetPlaceholder}
          mobileLink="https://public.tableau.com/views/BudgetStory_15812725950890/BudgetStory?:display_count=y&publish=yes&:origin=viz_share_link"
          vizId="viz1581520997312">
          <param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' />
          <param name='embed_code_version' value='3' />
          <param name='site_root' value='' />
          <param name='name' value='BudgetStory_15812725950890/BudgetStory' />
          <param name='tabs' value='no' />
          <param name='toolbar' value='yes' />
          <param name='static_image' value='https://public.tableau.com/static/images/Bu/BudgetStory_15812725950890/BudgetStory/1.png' />
          <param name='animate_transition' value='yes' />
          <param name='display_static_image' value='yes' />
          <param name='display_spinner' value='yes' />
          <param name='display_overlay' value='yes' />
          <param name='display_count' value='yes' />
          <param name='filter' value='publish=yes' />
        </TableauEmbed>
      </article>
    </EmbedWrapper>
  )
}

export default StateOperatingBudget
