import React from 'react'

import "./IconFundingCircle.scss"

const IconFundingCircle = ({ height, modifiers }) => {
  return (
    <svg
      className={`icon icon-funding-circle ${modifiers}`}
      xmlns="https://www.w3.org/2000/svg"
      viewBox="0 0 146 146"
      style={{height}}
    >
      <path
        className="icon-funding-circle__federal-funds"
        d="M71.5,72V3.5C33.8,4.3,3.5,35.1,3.5,73c0,14.2,4.3,27.5,11.6,38.5L71.5,72z"
      />
      <path
        className="icon-funding-circle__general-funds"
        d="M74.5,72l56.4,39.5c7.3-11,11.6-24.2,11.6-38.4c0-37.9-30.3-68.7-68-69.5V72z"
      />
      <path
        className="icon-funding-circle__cash-funds"
        d="M73,74.6L16.9,114c12.6,17.3,33.1,28.5,56.1,28.5c23.1,0,43.5-11.2,56.1-28.6L73,74.6z"
      />
      <path
        className="icon-funding-circle__outline"
        d="M73,1c19.2,0,37.3,7.5,50.9,21.1C137.5,35.7,145,53.8,145,73s-7.5,37.3-21.1,50.9C110.3,137.5,92.2,145,73,145
        s-37.3-7.5-50.9-21.1C8.5,110.3,1,92.2,1,73s7.5-37.3,21.1-50.9C35.7,8.5,53.8,1,73,1 M73,0C32.7,0,0,32.7,0,73s32.7,73,73,73
        s73-32.7,73-73S113.3,0,73,0L73,0z"
      />
    </svg>
  )
}

IconFundingCircle.defaultProps = {
  height: "8rem",
  modifiers: ""
}

export default IconFundingCircle
