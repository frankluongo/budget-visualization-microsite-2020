import React from "react";

import whereDoMyTaxesGoGraphic from "../../images/Where-Do-My-Taxes-and-Fees-Go-Infographic.pdf"

const exploreRevenueContent = {
  heading: 'Explore State Revenue',
  whereDoTaxesGoHeading: 'Where Do My Taxes and Fees Go?',
  whereDoTaxesGoText: (
    <>
      The taxes and fees for government services that you pay each year fund federal, state, and local government programs. Combining all of these taxes and fees, the graphic below illustrates where the average $1 in taxes and fees goes.
      <br />
      <br />
      A little less than a quarter of your taxes and fees is collected by the state government.
    </>
  ),
  seeMoreText: (
    <a className="link" href={whereDoMyTaxesGoGraphic} target="_blank" rel="noopener noreferrer">See More (Infographic & Source Data)</a>
  ),
  stateRevSourcesHeading: 'State Revenue Sources',
  stateRevSourcesText: (
    <>
      Funding for Colorado’s state budget comes from many different sources, including state taxes and fees, and funding from the federal government. The biggest source of revenue for the state are income taxes and sales taxes. Detailed information about each state tax can be found in the <a className="link" href="https://leg.colorado.gov/agencies/legislative-council-staff/colorado-online-tax-handbook" target="_blank" rel="noopener noreferrer" title="View the online tax handbook">Online Tax Handbook</a>.
      <br />
      <br />
      Examples of fees include state vehicle registration fees, higher education tuition, state parks fees, and occupational licensing fees.
    </>
  ),
}

export default exploreRevenueContent
