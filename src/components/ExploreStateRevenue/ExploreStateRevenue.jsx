import React from 'react'
import Section from '../Section'

import TextBlock from "../TextBlock";
import { Paragraph, PrimaryHeading, SecondaryHeading } from "../Text";
import WhereDoMyTaxesGoImage from "./Visualizations/WhereDoMyTaxesGoImage"

import data from "./exploreRevenueContent";
import StateTaxesStoryGraphic from './Visualizations/StateTaxesStoryGraphic';


const ExploreStateRevenue = () => {

  return (
    <Section number={5} id="explore-state-revenue" modifiers="bg-gray-1">
      <TextBlock>
        <article>
          <PrimaryHeading headingText={data.heading} />
        </article>
      </TextBlock>

      <TextBlock>
        <article>
          <SecondaryHeading>
            {data.whereDoTaxesGoHeading}
          </SecondaryHeading>
          <Paragraph>
            {data.whereDoTaxesGoText}
          </Paragraph>
          <div className="p m-xsmall-top-2 p-xsmall-inline-0 p-medium-inline-2">
            <WhereDoMyTaxesGoImage />
          </div>
          <Paragraph>
            {data.seeMoreText}
          </Paragraph>
        </article>
      </TextBlock>

      <TextBlock>
        <article>
          <SecondaryHeading>
            {data.stateRevSourcesHeading}
          </SecondaryHeading>
          <Paragraph>
            {data.stateRevSourcesText}
          </Paragraph>
        </article>
      </TextBlock>
      <StateTaxesStoryGraphic />

    </Section>
  )
}

export default ExploreStateRevenue
