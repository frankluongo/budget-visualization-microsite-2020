import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import image from "../../../images/where-do-my-taxes-go.png";

const WhereDoMyTaxesGoImage = () => {
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "where-do-my-taxes-go.png" }) {
        childImageSharp {
          fluid(maxWidth: 900) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return (
    <div>
      <Img fluid={data.placeholderImage.childImageSharp.fluid} />
      <a className="link" href={image} target="_blank" rel="noopener noreferrer">View Full Image</a>
    </div>
  )

}

export default WhereDoMyTaxesGoImage
