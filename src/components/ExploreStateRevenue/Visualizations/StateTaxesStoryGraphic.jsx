import React from 'react'
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

import EmbedWrapper from "../../EmbedWrapper"
import TableauEmbed from "../../TableauEmbed"

const GraphicPlaceholderImg = () => {
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "tax-story.png" }) {
        childImageSharp {
          fluid(maxWidth: 900) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return <Img fluid={data.placeholderImage.childImageSharp.fluid} />
}

const StateTaxesStoryGraphic = () => {
  return (
    <EmbedWrapper>
      <article>
        <TableauEmbed
          aspectRatio={0.65}
          MobileImg={GraphicPlaceholderImg}
          mobileLink="https://public.tableau.com/views/StateTaxesStory_15812795242480/Story1?:display_count=y&:origin=viz_share_link"
          vizId="viz1583861382932">
          <param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' /> <param name='embed_code_version' value='3' /> <param name='site_root' value='' /><param name='name' value='StateTaxesStory_15812795242480&#47;Story1' /><param name='tabs' value='no' /><param name='toolbar' value='yes' /><param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;St&#47;StateTaxesStory_15812795242480&#47;Story1&#47;1.png' /> <param name='animate_transition' value='yes' /><param name='display_static_image' value='yes' /><param name='display_spinner' value='yes' /><param name='display_overlay' value='yes' /><param name='display_count' value='yes' />
        </TableauEmbed>
      </article>
    </EmbedWrapper>
  )
}

export default StateTaxesStoryGraphic
