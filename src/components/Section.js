import React, { useContext, useEffect } from 'react'
import AppContext from "../context/AppContext";

import "../styles/components/section.scss"

const Section = ({ children, id, modifiers }) => {
  const { updateActivePage } = useContext(AppContext);
  const sectionRef = React.useRef(null);

  useEffect(observerSection, []);

  return (
    <section className={`section ${modifiers}`} id={id} ref={sectionRef}>
      <div className="section__container container">
        {children}
      </div>
    </section>
  )

  function observerSection() {
    if (sectionRef.current.id === 'introduction') return;
    const options = {
      rootMargin: '0px',
      threshold: 0.25
    }
    const observer = new IntersectionObserver(handleSectionVisibility, options);
    observer.observe(sectionRef.current)

    function handleSectionVisibility([{ isIntersecting }]) {
      if (isIntersecting) {
        updateActivePage(sectionRef.current.id)
      }
    }
  }
}

Section.defaultProps = {
  children: "",
  id: "",
  modifiers: ""
}

export default Section
