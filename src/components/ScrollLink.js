import React, { useContext } from 'react'

import { debounce } from "lodash"

import AppContext from "../context/AppContext";

const ScrollLink = ({ addedFn, classes, modifiers, sectionLink, children }) => {
  const { activePage, updateActivePage } = useContext(AppContext);

  return (
    <a
      href={`#${sectionLink}`}
      className={`${classes} ${modifiers} ${sectionLink === activePage ? 'active' : ''}`}
      onClick={handleNavClick}
    >
      {children}
    </a>
  )

  function handleNavClick(event) {
    event.preventDefault();
    const sectionTarget = event.currentTarget.getAttribute('href').replace("#", '');
    const sectionTargetOffset = document.querySelector(event.currentTarget.getAttribute('href')).offsetTop;
    window.addEventListener('scroll', debounce(() => updateActivePage(sectionTarget), 300), { once: true })
    window.scrollTo({
      top: sectionTargetOffset,
      left: 0,
      behavior: 'smooth'
    });
    addedFn();
  }
}

ScrollLink.defaultProps = {
  addedFn: () => { },
  children: 'Link!',
  classes: "link",
  modifiers: "",
  sectionLink: "",
}

export default ScrollLink
