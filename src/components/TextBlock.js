import React from 'react'

import "../styles/components/text-block.scss"

const TextBlock = ({ children, modifiers }) => {
  return (
    <section className={`text-block ${modifiers}`}>
      {children}
    </section>
  )
}

TextBlock.defaultProps = {
  modifiers: ''
}

export default TextBlock
