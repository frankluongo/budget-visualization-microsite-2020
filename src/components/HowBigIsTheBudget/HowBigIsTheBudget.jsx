import React from 'react'
import Section from '../Section'

import TextBlock from "../TextBlock";
import { IntroText, Paragraph, PrimaryHeading, SecondaryHeading, TertiaryHeading } from "../Text";
import data from "./howBigIsTheBudgetContent"
import BudgetGraphic from "./Visualizations/BudgetGraphic"
import StateBudgetVsGeneralFunds from './StateBudgetVsGeneralFunds';


const HowBigIsTheBudget = () => {

  return (
    <Section number={3} modifiers="bg-gray-1" id="how-big-is-the-budget">
      <TextBlock>
        <article>
          <PrimaryHeading headingText={data.heading} />
          <IntroText>
            {data.introText}
          </IntroText>
          <div className="p m-xsmall-top-2">
            <BudgetGraphic />
          </div>
          <Paragraph>
            {data.seeMoreLink}
          </Paragraph>
        </article>
      </TextBlock>

      <TextBlock>
        <aside className="img-wrapper">
          <div className="p-xsmall-right-2 m-xsmall-top-2">
            <div className="p-xsmall-left-6">
              <StateBudgetVsGeneralFunds />
            </div>
          </div>
        </aside>
        <article>
          <SecondaryHeading>
            {data.whatIsTheBudgetHeading}
          </SecondaryHeading>
          <Paragraph>
            {data.whatIsTheBudgetText}
          </Paragraph>
          <TertiaryHeading>
            {data.transfersToLocalGovHeading}
          </TertiaryHeading>
          <Paragraph>
            {data.transfersText}
          </Paragraph>
          <TertiaryHeading>
            {data.doubleCountedDollarsHeading}
          </TertiaryHeading>
          <Paragraph>
            {data.doubleCountedText}
          </Paragraph>
        </article>
      </TextBlock>
    </Section>
  )
}

export default HowBigIsTheBudget
