import React from "react";

import howBigisTheBudget from "../../images/How-Big-is-the-Colorado-State-Budget-Infographic.pdf"

const howBigIsTheBudgetContent = {
  heading: 'How Big is the State Budget?',
  introText: 'Just looking at dollar amounts, state spending represents about 13% of the state economy.  But that isn’t the whole story…',
  seeMoreLink: (
    <a className="link" href={howBigisTheBudget} target="_blank" rel="noopener noreferrer">See More (Infographic and Source Data)</a>
  ),
  whatIsTheBudgetHeading: 'What is “the budget”?',
  whatIsTheBudgetText: (
    <>
      When most people talk about the state budget they are referring to the state budget that is set annually by the state legislature. In FY 2019-20, the state budget totals about $32.5 billion.
      <br />
      <br />
      Of the $32.5 billion, $12.2 billion is from the General Fund, which supports basic state-funded services, such as K-12 education, prisons, courts, and public assistance. Most of the General Fund revenue comes from income and sales taxes. The remainder of the state budget comes from cash funds and federal funds, which must be used for specific purposes as determined by state and federal law.
      <br />
      <br />
      Other forms of state spending occur outside of the state budget. For example, federal funds for food assistance programs and some federal and private funding for higher education research projects are not “appropriated” by the state legislature.
      <br />
      <br />
      In addition, some amounts are not spent by the state.
    </>
  ),
  howMuchSpentByStateHeading: 'How much of the budget is spent by the state?',
  transfersToLocalGovHeading: 'Transfers to Local Governments',
  transfersText: (
    <>
      About a quarter of state operating funds are allocated to local governments, including school districts, cities, and counties for spending on education, transportation, and human services programs.
    </>
  ),
  doubleCountedDollarsHeading: 'Double-Counted Dollars',
  doubleCountedText: 'About $2.1 billion (6.5%) in the FY 2019-20 state operating budget are “reappropriated” funds, meaning these funds are allocated to one state department, but actually spent by another.  In a nutshell, these funds are double-counted and can even be counted more than twice in the state budget.'
}

export default howBigIsTheBudgetContent
