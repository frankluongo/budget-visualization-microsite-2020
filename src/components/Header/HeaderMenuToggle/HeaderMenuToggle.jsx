import React from 'react'

import "./HeaderMenuToggle.scss"

const HeaderMenu = ({ toggleAction, toggledState }) => {

  return (
    <div className="header__menu-toggle">
      <button
        aria-label="Display Navigation Menu"
        className="burger-menu"
        data-toggle={toggledState}
        onClick={toggleAction}
      >
        <div className="burger-menu__bar bar-1" />
        <div className="burger-menu__bar bar-2" />
        <div className="burger-menu__bar bar-3" />
      </button>
    </div>
  )
}

export default HeaderMenu
