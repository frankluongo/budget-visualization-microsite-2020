import React, { useState, useContext, useEffect } from "react"

import "./Header.scss"

import HeaderMenuToggle from "./HeaderMenuToggle";
import HeaderLink from "./HeaderLink/HeaderLink";
import HeaderMenu from "./HeaderMenu";
import AppContext from "../../context/AppContext";
import NavigationList from "./NavigationList";


const Header = ({ mainSiteUrl }) => {
  const { height: windowHeight, width: windowWidth } = useContext(AppContext);
  const [isActive, setIsActive] = useState(false);

  useEffect(checkWindowWidth, [windowWidth]);

  return (
    <header
      className="header"
      aria-expanded={isActive}
      style={{ height: (isActive ? `${windowHeight}px` : "96px") }}
    >
      <div className="header__content container">
        <HeaderLink isActive={isActive} mainSiteUrl={mainSiteUrl} />
        <HeaderMenuToggle toggleAction={toggleMenu} toggledState={isActive} />
        <NavigationList modifiers="navigation-list--desktop" />
      </div>
      <HeaderMenu isActive={isActive} toggleAction={toggleMenu} />
    </header>
  )

  function toggleMenu() {
    setIsActive(!isActive);
  }

  function checkWindowWidth() {
    if (windowWidth >= 1440) {
      setIsActive(false)
    }
  }
}


Header.defaultProps = {
  mainSiteUrl: `//leg.colorado.gov/`
}

export default Header
