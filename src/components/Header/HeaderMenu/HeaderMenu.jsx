import React from 'react'

import "./HeaderMenu.scss"

import NavigationList from "../NavigationList"

const HeaderMenu = ({ mainSiteUrl, isActive, toggleAction }) => {

  return (
    <div className={`header__expanded-content container ${isActive ? 'nav-open' : ''}`}>
      <div className="header-expanded__heading text-align-center">
        <hr className="bg-lcs-blue-2" />
        <a className="link" href={mainSiteUrl} rel='noopener noreferrer' target="_blank">
          Return to Main Site
          </a>
      </div>
      <div className="header-expanded__nav-list m-xsmall-top-2 m-medium-top-3">
        <NavigationList modifiers="header-expanded__navigation" toggleAction={toggleAction} />
      </div>
    </div>
  )
}

HeaderMenu.defaultProps = {
  mainSiteUrl: `//leg.colorado.gov/`
}

export default HeaderMenu
