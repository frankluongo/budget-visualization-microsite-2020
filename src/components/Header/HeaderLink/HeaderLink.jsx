import React, { useContext, useRef } from 'react'

import HeaderImg from "../HeaderImg"

import "./HeaderLink.scss"
import WindowContext from '../../../context/AppContext'

const HeaderLink = ({ isActive, mainSiteUrl, subtitle, title }) => {
  const { width: windowWidth } = useContext(WindowContext);
  const headingElement = useRef(null);

  return (
    <div className="header__link-wrapper">
      <a
        aria-label={title}
        className="header__link"
        href={mainSiteUrl}
        rel='noopener noreferrer'
        target="_blank"
        style={{ transform: (isActive ? `translate3d(${calculateHeaderLinkAnimation()}px,0px,0px)` : ``) }}
        ref={headingElement}
        title={title}
      >
        <h1 className="header__h1">
          <HeaderImg />
          <div className="header-h1__text">
            <div className="p small header-h1__subtitle text-lcs-blue-5">
              <strong>{subtitle}</strong>
            </div>
            <div className="h3 header-h1__title text-gray-8">
              {title}
            </div>
          </div>
        </h1>
      </a>
    </div>
  )

  function calculateHeaderLinkAnimation() {
    const { current: heading } = headingElement;
    const offset = heading.offsetLeft;
    const elementWidth = heading.getBoundingClientRect().width;
    return ((windowWidth - elementWidth) / 2) - offset;
  }
}

HeaderLink.defaultProps = {
  isActive: false,
  mainSiteUrl: `//leg.colorado.gov/`,
  subtitle: `Second Regular Session | 72nd General Assembly`,
  title: `Colorado General Assembly`,
}

export default HeaderLink
