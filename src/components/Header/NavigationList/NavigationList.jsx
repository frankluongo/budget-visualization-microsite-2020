import React from 'react'

import ScrollLink from "../../ScrollLink";

import "./NavigationList.scss"

const NavigationList = ({ links, modifiers, toggleAction }) => {
  return (
    <ul className={`navigation-list ${modifiers}`}>
      {links.map(({ title, identifier }) =>
        <li className="navigation-list__item" key={`nav-${identifier}`}>
          <ScrollLink
            addedFn={toggleAction.bind(null, false)}
            classes="navigation-list__link h1 text-lcs-blue-4"
            sectionLink={identifier}
          >
            {title}
          </ScrollLink>
        </li>
      )}
    </ul>
  )
}

NavigationList.defaultProps = {
  modifiers: "",
  links: [
    {
      title: "Budget Basics",
      identifier: "budget-basics"
    },
    {
      title: "How Big Is The State Budget?",
      identifier: "how-big-is-the-budget"
    },
    {
      title: "Explore The State Budget",
      identifier: "explore-the-state-budget"
    },
    {
      title: "Explore State Revenue",
      identifier: "explore-state-revenue"
    },
    {
      title: "What Is TABOR?",
      identifier: "what-is-tabor"
    },
    {
      title: "Further Reading",
      identifier: "further-reading"
    },
  ],
  toggleAction: () => { }
}

export default NavigationList
