import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

const HeaderImg = () => {
  const data = useStaticQuery(graphql`
    {
      headerImage: file(relativePath: {eq: "cga-logo-med.png"}) {
      childImageSharp {
      fixed(width: 64, height: 64) {
        ...GatsbyImageSharpFixed
      }
    }
  }
    }
  `)

  return <Img fixed={data.headerImage.childImageSharp.fixed} alt="Colorado General Assembly" />
}

export default HeaderImg
