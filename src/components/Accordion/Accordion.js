import React from "react";

import "./Accordion.scss"
import AccordionPanel from "./Panel";

const Accordion = ({ modifiers, panels }) => {
  return (
    <div className={`accordion ${modifiers}`}>
      {panels.map((panel, index) => (
        <AccordionPanel
          title={panel.title}
          content={panel.content}
          key={index}
        />
      ))}
    </div>
  );
};

Accordion.defaultProps = {
  modifiers: "",
  panels: [],
}

export default Accordion;
