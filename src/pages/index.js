import React, { useState } from "react"
import smoothscroll from 'smoothscroll-polyfill';

import useWindowSize from "../hooks/useWindowSize"

import Layout from "../components/layout"
import SEO from "../components/seo"
import BudgetBasics from "../components/BudgetBasics"
import HowBigIsTheBudget from "../components/HowBigIsTheBudget"
import ExploreStateBudget from "../components/ExploreStateBudget"
import ExploreStateRevenue from "../components/ExploreStateRevenue";
import WhatIsTABOR from "../components/WhatIsTABOR";
import FurtherResources from "../components/FurtherResources";
import IntroSection from "../components/IntroSection";
import AppContext from "../context/AppContext";

const IndexPage = () => {
  if (typeof window !== 'undefined') {
    smoothscroll.polyfill();
  }
  const [activePage, updateActivePage] = useState(null);
  const [width, height] = useWindowSize();

  return (
    <AppContext.Provider value={{ activePage, updateActivePage, width, height }}>
      <Layout>
        <SEO title="Home" />
        <IntroSection />
        <BudgetBasics />
        <HowBigIsTheBudget />
        <ExploreStateBudget />
        <ExploreStateRevenue />
        <WhatIsTABOR />
        <FurtherResources />
      </Layout>
    </AppContext.Provider>
  )
}

export default IndexPage
