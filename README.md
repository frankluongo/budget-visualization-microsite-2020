# The Colorado State Budget Visualization Microsite

## Important Commands

### Development

```bash
yarn develop
```

### Build For Production

```bash
gatsby build --prefix-paths --no-uglify
```

After you run the build command, Gatsby will generate or update the `public` folder. Create a .zip of that folder, and send it to Bart Lantz.

## Caveats

If there's an issue with the build, delete the `.cache.` and `public` folders, and run the build command again
