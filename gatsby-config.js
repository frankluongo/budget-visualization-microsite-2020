module.exports = {
  pathPrefix: `/explorebudget`,
  siteMetadata: {
    title: `Colorado State Budget`,
    description: `Get an in-depth look into how the State of Colorado acquires and distributes its state budget.`,
    author: `@frankluongo`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `colorado-state-budget-visualization`,
        short_name: `budget-visualization`,
        start_url: `/`,
        background_color: `#186ea8`,
        theme_color: `#186ea8`,
        display: `minimal-ui`,
        icon: `src/images/cga-logo-med.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-sass`,
  ],
}
