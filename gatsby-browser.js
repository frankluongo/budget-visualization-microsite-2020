import "./src/styles/application.scss"

require("typeface-public-sans")
require("typeface-merriweather")

export const onClientEntry = () => {
  if (typeof window !== 'undefined') {
    if (!(`IntersectionObserver` in window)) {
      import(`intersection-observer`)
    }
  }
}
